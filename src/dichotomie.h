/**
 * @file dichotomie.h
 * @author Thibaut Goullet  (thibaut.goullet@cpe.com)
 * @brief 
 * @version 0.1
 * @date 2021-10-24
 * 
 * @copyright Copyright (c) 2021
 * 
 */

/**
* Recherche par dichotomie dans un tableau d'entiers
* @param array The array of values
* @param size_t The size of the array
* @param value The value to find
* @return The position of the value found or -1
*/


int find_by_dichotomy(int array[], int size_t, int value );