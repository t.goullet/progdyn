/**
 * @file knapsack.h
 * @author Thibaut Goullet  (thibaut.goullet@cpe.com)
 * @brief 
 * @version 0.1
 * @date 2021-10-24
 * 
 * @copyright Copyright (c) 2021
 * 
 */


typedef struct {
    int valeur;
    float poids;
} Object;

/**
 * @brief triage d'objet en fonction de la valeur et du poids
 * @param objets_not_sorted of Object
 * @param size of the tab
 */
void tri_object(Object *objets_not_sorted, int size){


/**
 * @brief mets les objets dans le sac
 * @param objets tableau d'objets
 * @param size taille du tableau
 * @param bag taille du sac
 * @return Object* liste les objets utilisés
 */
Object *knapsack(Object *objets, int size, int bag);
