/**
 * @file main.c
 * @author Thibaut Goullet  (thibaut.goullet@cpe.com)
 * @brief 
 * @version 0.1
 * @date 2021-10-24
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include "dichotomie.h"
#include "knapsack.h"
#include "assert.h"
#include <stdio.h>

int main()
{


	///// EXERCICE 1 /////

	int tab[20] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
	int taille = 20;
	int value = 10;

	int index = find_by_dichotomy(tab, taille, value);

	if (index != -1)
	{
		printf("\n La valeur %i est à la  %i position \n", value, index );
	}
	else
	{
		printf("\n La valeur %i n'est pas dans le tableau\n", value);
	}

	
	///// EXERCICE 2 /////


	Object o1,o2,o3;
	o1.valeur = 7;
	o1.poids = 6;
	o2.valeur = 5;
	o2.poids = 5;
	o3.valeur = 5;
	o3.poids = 5;

	Object tab_objects[] = {o1,o2,o3};
	int size = 3;

	int bag = 10;
	Object *objects_sorted = knapsack(tab_objects, size, bag);

	///// EXERCICE 3 /////

int taille = 3;

	int matrice = create_matrice(taille);
	show_matrice(matrice, taille);

	return (0);
}