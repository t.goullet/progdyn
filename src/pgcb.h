/**
 * @file pgcb.h
 * @author Thibaut Goullet  (thibaut.goullet@cpe.com)
 * @brief 
 * @version 0.1
 * @date 2021-10-24
 * 
 * @copyright Copyright (c) 2021
 * 
 */


/**
 * @brief Creation de l'objet matrice
 * 
 * @param taille 
 * @return int** 
 */
int **create_matrice(int taille);

/**
 * @brief affichage de la matrice
 * 
 * @param matrice 
 * @param taille 
 */
void show_matrice(int **matrice, int taille);