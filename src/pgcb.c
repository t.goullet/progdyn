/**
 * @file pgcb.c
 * @author Thibaut Goullet  (thibaut.goullet@cpe.com)
 * @brief 
 * @version 0.1
 * @date 2021-10-24
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include "pgcb.h"

/**
 * @brief Creation de l'objet matrice
 * 
 * @param taille 
 * @return int** 
 */

int **create_matrice(int taille){
    int matrice[taille][taille];
    for (int i = 0; i < taille; i++)
    {
        for (int j = 0; j < taille; i++)
        {
            matrice[i][j] = 0;
        }
        
    }
    int nb_rand=rand()%taille+1;
    for (int k = 0; k < nb_rand; k++)
    {
         matrice[rand()%taille+0][rand()%taille+0] = 1;
    }
    
    return matrice;
}

/**
 * @brief affichage de la matrice
 * 
 * @param matrice 
 * @param taille 
 */
void show_matrice(int **matrice, int taille){
    for (int i = 0; i < taille; i++){
        for (int j = 0; i < taille; j++){
            printf("%d   ", matrice[i][j]);
        }
    printf("\n");
    }     
}