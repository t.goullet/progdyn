/**
 * @file dichotomie.c
 * @author Thibaut Goullet  (thibaut.goullet@cpe.com)
 * @brief 
 * @version 0.1
 * @date 2021-10-24
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include "dichotomie.h"

/**
 * @brief Recherche par dichotomie dans un tableau d'entiers
 * @param array 
 * @param size_t 
 * @param value 
 * @return int 
 */

int find_by_dichotomy(int array[], int size_t, int value ){
    int index_min = 0;
    int index_max = size_t;

    while (index_min <= index_max)
    {
        int index = (index_min + index_max) >> 1;
        if (array[index] == value)
        {
            return index;
        }       
        else if (array[index] > value)
        {
            index_max = index - 1;
        }
        else
        {
            index_min = index + 1;
        }
    }
    int not_find = -1;
    return not_find;
}