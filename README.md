# ProgDyn

TP 4 - ProgDyn

Language = C

Auteur : GOULLET Thibaut

## Objectif du TP

Exercice 1 : Implémentation d'un algorithme de dichotomie.
Exercice 2 : Résolution du problème du sac à dos avec un algorithme glouton.
Exercice 3 : Résolution du problème du plus grand carré blanc
